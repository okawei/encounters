import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Layouts/Home'
import Dashboard from '@/components/Layouts/Dashboard'
import StyleGuide from '@/components/Internal/StyleGuide'
import store from '@/vuex/store'

Vue.use(Router)

var setPartyCode = function(to, from, next){
  if(to.params.partyCode){
    store.dispatch('getParty', to.params.partyCode);
    store.dispatch('getAllEnumerators');
  }
  next()
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/style-guide',
      name: 'StyleGuide',
      component: StyleGuide
    },
    {
      path: '/:partyCode',
      name: 'Party',
      component: Dashboard,
      beforeEnter: setPartyCode
    }
  ]
})

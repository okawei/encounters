import Vuex from 'vuex'
import Vue from 'vue'
import PartyActions from './../vuex/api/partyActions'
import MonsterActions from './../vuex/api/monsterActions'
import EncounterActions from './../vuex/api/encounterActions'
import PlayerCharacterActions from './../vuex/api/playerCharacterActions'
import EventActions from './../vuex/api/eventsActions'
import SpellActions from './../vuex/api/spellActions'
import EnumeratorActions from './../vuex/api/enumeratorActions'

import router from './../router'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 0,
    monsters: [],
    monstersList: [],
    spellsList: [],
    monster: {},
    spell: {},
    races: [],
    classes: [],
    alignments: [],
    party: {},
    playerCharacters: [],
    playerCharacter: {},
    currentEncounter: {},
    encounterEntities: [],
    latestPlayer: {},
    events: [],
    currentEncounterEntityId: undefined,
  },
  mutations: {
    GET_ENUMERATORS(state, enumerators){
      state.enumerators = enumerators
    },
    GET_MONSTERS(state, monsters) {
      state.monsters = monsters
    },
    LIST_MONSTERS(state, monsters) {
      state.monstersList = monsters
    },
    GET_MONSTER(state, monster) {
      state.monster = monster
    },
    CREATE_MONSTER(state, monster){
      state.monstersList.push({
        id: monster.body.id,
        name: monster.body.monster.name
      });
      state.latestMonster = monster.body
    },
    DELETE_MONSTER(state, response){
      state.monstersList = state.monstersList.filter(function (monster) {
        return monster.id != response.body.monster_id
      })
    },
    GET_SPELLS(state, spells) {
      state.spells = spells
    },
    LIST_SPELLS(state, spells) {
      state.spellsList = spells
    },
    GET_SPELL(state, spell) {
      state.spell = spell
    },
    CREATE_PARTY(state, party) {
      state.party = party.body
      router.push(state.party.hash)
    },
    CREATE_ENCOUNTER(state, encounter) {
      state.currentEncounter = encounter.body
      state.encounterEntities = []
    },
    ADD_ENCOUNTER_MONSTER(state, encounterMonster) {
      encounterMonster.type = 'monster';
      state.encounterEntities.push(encounterMonster.body);
    },
    ADD_ENCOUNTER_PLAYER(state, encounterPlayer) {
      encounterPlayer.type = 'player';
      state.encounterEntities.push(encounterPlayer.body);
    },
    GET_RACES(state, races) {
      state.races = races;
    },
    GET_CLASSES(state, classes) {
      state.classes = classes
    },
    GET_ALIGNMENTS(state, alignments) {
      state.alignments = alignments;
    },
    CREATE_PLAYER(state, player) {
      state.playerCharacters.push(player.body)
      state.latestPlayer = player.body;
    },
    UPDATE_PLAYER(state, player) {
      state.latestPlayer = player.body;
      state.encounterEntities = state.encounterEntities.map(function(entity){
         if(entity.type == 'player_character' && entity.player_character_id == player.body.id){
           entity.player_character = player.body.player_character;
         }
         return entity;
      })
    },
    GET_PLAYERS(state, players) {
      state.playerCharacters = players;
    },
    GET_PLAYER(state, player) {
      state.playerCharacter = player;
    },
    DELETE_PLAYER(state, player){
      state.playerCharacters = state.playerCharacters.filter(function(playerCharacter){
        return playerCharacter.id != player.body.id;
      })
      state.encounterEntities = state.encounterEntities.filter(function(entity){
        if(entity.type == 'player_character' && entity.player_character_id == player.body.id){
          return false;
        }
        return true;
      });
    },
    GET_PARTY(state, party) {
      state.party = party
      state.playerCharacters = party.player_characters;
      state.currentEncounter = party.latest_encounter
      state.encounterEntities = party.latest_encounter_entities;
    },
    GET_EVENTS(state, events) {
      state.events = events
    },
    CREATE_EVENT(state, event) {
      state.events.push(event)
    },
    SET_CURRENT_ENCOUNTER_ENTITY(state, id){
      state.currentEncounterEntityId = id;
    },
    REMOVE_ENCOUNTER_ENTITY(state, id){
      state.encounterEntities = state.encounterEntities.filter(function(entity){
        return entity.id != id.body.id
      });
    },
    ROLLED_INITIATIVE(state, response){
      state.encounterEntities = response.body;
    },
    UPDATED_ENCOUNTER_ENTITY(state, entity){
      console.log(entity)
    },
    API_FAILURE(state, error) {
      console.log(error);
      alert('API error!')
    }
  },
  actions: Object.assign(PartyActions, MonsterActions, PlayerCharacterActions, EncounterActions, EventActions, SpellActions, EnumeratorActions)
})

export default store;

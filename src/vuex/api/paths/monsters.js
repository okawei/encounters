var base = '/api/monsters';
const routes = {
    GET_MONSTERS: base,
    GET_MONSTER: base,
    LIST_MONSTERS: base+'?list=true'
}


export default routes;
var base = '/api/spells';
const routes = {
    CREATE_SPELL: base,
    GET_SPELLS: base,
    LIST_SPELLS: base+'?list=true',
    GET_SPELL: base
}

export default routes;
var base = '/api/player_characters';
const routes = {
    CREATE_PLAYER: base,
    GET_PLAYERS: base,
    GET_PLAYER: base
}

export default routes;
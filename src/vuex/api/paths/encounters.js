var base = '/api/encounters';
const routes = {
    GET_ENCOUNTERS: base,
    GET_ENCOUNTER: base,
    CREATE_ENCOUNTER: base,
    ADD_ENCOUNTER_MONSTER: '/api/encounter/monsters',
    ADD_ENCOUNTER_PLAYER: '/api/encounter/player_characters',
    GET_ENCOUNTER_MEMBERS: '/api/encounter/members',
    ROLL_INITIATIVE: '/api/encounters'
}


export default routes;

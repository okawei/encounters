import Monsters from './paths/monsters'
import Parties from './paths/parties'
import Races from './paths/races'
import Classes from './paths/classes'
import Alignments from './paths/alignments'
import Players from './paths/players'
import Encounters from './paths/encounters'
import Events from './paths/events'
import Spells from './paths/spells'
import Enumerators from './paths/enumerators';

var baseUrl = process.env.URL_BASE_PATH;
var paths = Object.assign(Monsters, Parties, Races, Alignments, Players, Encounters, Events, Spells, Classes, Enumerators)

for (var property in paths) {
    if (paths.hasOwnProperty(property)) {
        paths[property] = baseUrl+paths[property]
    }
}

export default paths;

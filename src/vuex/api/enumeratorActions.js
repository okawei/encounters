import api from './../utils/api'
import Paths from './paths'

const actions = {
  getAllEnumerators (context) {
    return api.get(Paths.GET_ENUMERATORS)
      .then((response) => context.commit('GET_ENUMERATORS', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
}

export default actions;

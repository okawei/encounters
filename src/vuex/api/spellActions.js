import api from './../utils/api'
import Paths from './paths'

const actions = {
    getSpells (context)  {
        if(context.state.spells.length){
            return;
        }
        return api.get(Paths.GET_SPELL)
            .then((response) => context.commit('GET_SPELL', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    listSpells (context)  {
        if(context.state.spellsList.length){
            return;
        }
        return api.get(Paths.LIST_SPELLS)
            .then((response) => context.commit('LIST_SPELLS', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    getSpell(context, id) {
        return api.get(Paths.GET_SPELL+'/'+id)
            .then((response) => context.commit('GET_SPELL', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
}

export default actions;
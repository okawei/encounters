import api from './../utils/api'
import Paths from './paths'

const actions = {
    createParty (context)  {
        return api.post(Paths.CREATE_PARTY)
            .then((response) => context.commit('CREATE_PARTY', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    getParty (context, partyCode)  {
        return api.get(Paths.GET_PARTY+'/'+partyCode)
            .then((response) => context.commit('GET_PARTY', response))
            .catch((error) => context.commit('API_FAILURE', error));
    }
}

export default actions;
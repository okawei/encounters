import api from './../utils/api'
import Paths from './paths'

const actions = {
  getRaces(context) {
    if (context.state.races.length) {
      return;
    }
    return api.get(Paths.GET_RACES)
      .then((response) => context.commit('GET_RACES', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  getClasses(context) {
    if (context.state.classes.length){
      return
    }
    return api.get(Paths.GET_CLASSES)
      .then((response) => context.commit('GET_CLASSES', response))
      .catch((error) => context.commit('API_FAILURE', error))
  },
  getAlignments(context) {
    if (context.state.alignments.length) {
      return;
    }
    return api.get(Paths.GET_ALIGNMENTS)
      .then((response) => context.commit('GET_ALIGNMENTS', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  createPlayer(context, player) {
    return api.post(Paths.CREATE_PLAYER, player)
      .then((response) => context.commit('CREATE_PLAYER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  updatePlayer(context, player) {
    return api.put(Paths.CREATE_PLAYER+'/'+player.id, player)
      .then((response) => context.commit('UPDATE_PLAYER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  getPlayerCharacters(context, force = false) {
    if( (context.state.playerCharacters || !context.state.party.hash) && !force) {
      return;
    }
    return api.get(Paths.GET_PLAYERS + '?party_code=' + context.state.party.hash)
      .then((response) => context.commit('GET_PLAYERS', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  getPlayerCharacter(context, id) {
    return api.get(Paths.GET_PLAYER + '/' + id + '?party_code=' + context.state.party.hash)
      .then((response) => context.commit('GET_PLAYER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  deletePlayerCharacter(context, id) {
    return api.delete(Paths.GET_PLAYER + '/' + id + '?party_code=' + context.state.party.hash)
      .then((response) => context.commit('DELETE_PLAYER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
}

export default actions;

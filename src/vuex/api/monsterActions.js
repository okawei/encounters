import api from './../utils/api'
import Paths from './paths'

const actions = {
    getMonsters (context)  {
        if(context.state.monsters.length){
            return;
        }
        return api.get(Paths.GET_MONSTERS, {
          params: {
            party_code: context.state.party.hash
          }
        })
            .then((response) => context.commit('GET_MONSTERS', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    listMonsters (context)  {
        if(context.state.monstersList.length){
            return;
        }
        return api.get(Paths.GET_MONSTERS, {
          params: {
            party_code: context.state.party.hash,
            list: true
          }
        })
            .then((response) => context.commit('LIST_MONSTERS', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    getMonster(context, id) {
        return api.get(Paths.GET_MONSTER+'/'+id, {
          params: {
            party_code: context.state.party.hash
          }
        })
            .then((response) => context.commit('GET_MONSTER', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    createMonster(context, monster) {
      monster.party_code =  context.state.party.hash;
        return api.post(Paths.GET_MONSTER, monster)
            .then((response) => context.commit('CREATE_MONSTER', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    deleteMonster(context, monster) {

        return api.delete(Paths.GET_MONSTER+'/'+monster.id, {
          party_code: context.state.party.hash
        })
            .then((response) => context.commit('DELETE_MONSTER', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
}

export default actions;

import api from './../utils/api'
import Paths from './paths'

const actions = {
  createEncounter(context, encounterData) {
    if(!encounterData){
      encounterData = {};
    }
    encounterData.party_code = context.state.party.hash;
    return api.post(Paths.CREATE_ENCOUNTER, encounterData)
      .then((response) => context.commit('CREATE_ENCOUNTER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  addEncounterMonster(context, monsterData) {
    return api.post(Paths.ADD_ENCOUNTER_MONSTER + '?party_code=' + context.state.party.hash, monsterData)
      .then((response) => context.commit('ADD_ENCOUNTER_MONSTER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  addEncounterPlayer(context, monsterData) {
    return api.post(Paths.ADD_ENCOUNTER_PLAYER + '?party_code=' + context.state.party.hash, monsterData)
      .then((response) => context.commit('ADD_ENCOUNTER_PLAYER', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  removeEncounterEntity(context, data) {
    let path = Paths.ADD_ENCOUNTER_PLAYER;
    if(data.type == 'monster'){
      path = Paths.ADD_ENCOUNTER_MONSTER
    }
    return api.delete(path + '/' + data.id, {
      'party_code': context.state.party.hash,
      'encounter_id': data.encounter_id
    })
      .then((response) => context.commit('REMOVE_ENCOUNTER_ENTITY', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  selectEncounterEntity(context, id){
    context.commit('SET_CURRENT_ENCOUNTER_ENTITY', id);
  },
  rollInitiative(context, data){
    return api.post(Paths.ROLL_INITIATIVE + '/'+context.state.currentEncounter.id+'/roll_initiative', {
      entities: data,
      party_code: context.state.party.hash
    })
      .then((response) => context.commit('ROLLED_INITIATIVE', response))
      .catch((error) => context.commit('API_FAILURE', error));
  },
  updateEncounterEntity(context, entity){
    let endpoint = '';
    if(entity.type == 'player_character'){
      endpoint = Paths.ADD_ENCOUNTER_PLAYER
    } else {
      endpoint = Paths.ADD_ENCOUNTER_MONSTER
    }
    entity.party_code = context.state.party.hash
    return api.put(endpoint + '/'+entity.id, entity)
      .then((response) => context.commit('UPDATED_ENCOUNTER_ENTITY', entity))
      .catch((error) => context.commit('API_FAILURE', error));
  }
}

export default actions;

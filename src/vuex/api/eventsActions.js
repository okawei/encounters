import api from './../utils/api'
import Paths from './paths'

const actions = {
    createEvent (context, text) {
        return api.post(Paths.CREATE_EVENT, {
            text: text,
            party_code: context.state.party.hash,
            encounter_id: context.state.currentEncounter.id
        })
            .then((response) => context.commit('CREATE_EVENT', response.body))
            .catch((error) => context.commit('API_FAILURE', error));
    },
    getEvents (context) {
        return api.get(Paths.GET_EVENTS)
            .then((response) => context.commit('GET_EVENTS', response))
            .catch((error) => context.commit('API_FAILURE', error));
    },
}

export default actions;